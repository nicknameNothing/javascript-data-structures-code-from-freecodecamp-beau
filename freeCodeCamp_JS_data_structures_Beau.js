// video link: https://youtu.be/t2CEgPsws3U
/*
    This is my implementation of Beau's concise but
    useful JavaScript Data Structures and Algorithm
    YouTube tutorial. I've tried my best using 
    modern JavaScript syntax as much as I can.
    Feel free to use this as a reference or a
    freshup! :-)
*/

/* 1. Stack */

// You can use Array methods to implement
// a stack.
// eg. Palindrome ( rather simple and straightforward )
let letterStack = [];
let word = "racecar";
// let word = "freeCodeCamp";

let reversedWord = "";

for (let i = 0; i < word.length; i++) {
    letterStack.push(word[i]);
} // push letters into the stack

for (let i = 0; i < word.length; i++) {
    reversedWord += letterStack.pop();
} // pop letters from the stack

if (reversedWord === word) {
    console.log(word + " is a palindrome.");
} else console.log(word + " is not a palindrome");

// Create a Stack
let MyStack = function () {
    this.itemCount = 0;
    this.storage = {};

    this.push = function (item) {
        this.storage[this.itemCount] = item;
        this.itemCount++;
    };

    this.pop = function () {
        if (this.itemCount === 0) return undefined;

        this.itemCount--;
        let popItem = this.storage[this.itemCount];
        delete this.storage[this.itemCount];
        // note on delete operator:
        // delete Object.property;
        // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/delete
        return popItem;
    };

    this.size = function () {
        return this.itemCount;
    };

    this.peek = function () {
        return this.storage[this.itemCount - 1];
    };
};

// let testStack = new MyStack();

// testStack.push("a");
// testStack.push("b");
// testStack.push("c");
// console.log(testStack.peek());
// console.log(testStack.pop());
// console.log(testStack.peek());
// console.log(testStack.size());

/* 2. Set */
// Like an Array, but there's no duplicated
// items. Set is often used when checking whether
// an item is present. ES6 has a builtin Set Object.

function MySet() {
    let collection = [];

    this.has = function (item) {
        return collection.indexOf(item) !== -1; // true false
        // if the item is in the set, collection.indexOf(item)
        // will return the actual index of the item in the array
        // if not, it will return -1
    };

    this.showCollection = function () {
        return collection;
    };

    this.add = function (item) {
        if (!this.has(item)) {
            collection.push(item);
            return true;
        } else return false;
    };

    this.remove = function (item) {
        if (this.has(item)) {
            let itemIndex = collection.indexOf(item);
            // delete collection[itemIndex]; // does this work? nope, <1 empty slot>
            // alternative:
            collection.splice(itemIndex, 1);
            // note on Array.splice():
            // splice(start, deleteCount);
            // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/splice
            return true;
        } else return false;
    };

    this.size = function () {
        return collection.length;
    };

    // union of two sets, return a new set
    this.union = function (anotherSet) {
        let unionSet = new MySet();
        let firstSet = this.showCollection();
        let secondSet = anotherSet.showCollection();

        firstSet.forEach(function (item) {
            unionSet.add(item);
        });

        secondSet.forEach(function (item) {
            unionSet.add(item);
        });

        // note on Array.forEach(function(element))
        // executes a given function once for each array element
        // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/forEach

        return unionSet;
    };

    // intersection of two sets, return a new set
    this.intersection = function (anotherSet) {
        let intersectionSet = new MySet();
        let firstSet = this.showCollection(); // why not this.collection?

        firstSet.forEach(function (item) {
            if (anotherSet.has(item)) {
                intersectionSet.add(item);
                // intersectionSet.push(item);
                // note on Set.add(item): add item to the end
                // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Set/add
            }
        });

        return intersectionSet;
    };

    // difference of a new set, return a new set
    // item in the first set but not in the second one
    this.difference = function (anotherSet) {
        let differenceSet = new MySet();
        let firstSet = this.showCollection();

        firstSet.forEach(function (item) {
            if (!anotherSet.has(item)) {
                differenceSet.add(item);
            }
        });

        return differenceSet;
    };

    // test if the FIRST set is a subset of the SECOND one
    this.isSubSet = function (anotherSet) {
        let firstSet = this.showCollection();

        return firstSet.every(function (item) {
            return anotherSet.has(item);
        });
        // note on Array.every()
        // check whether all the elements in the array can pass the test function
        // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/every
    };
}

// let mySetA = new MySet();
// let mySetB = new MySet();
// mySetA.add("A");
// mySetA.add("B");

// mySetB.add("A");
// mySetB.add("B");
// mySetB.add("C");

// mySetA.isSubSet(mySetB);
// mySetA.add("D");
// mySetA.showCollection();
// mySetA.has("D");

// mySetA.remove("D");
// mySetA.showCollection();
// mySetA.has("D");
// mySetA.size();

// let testSet1 = mySetA.union(mySetB);
// testSet1.showCollection();

// let testSet2 = mySetA.intersection(mySetB);
// testSet2.showCollection();

// let testSet3 = mySetA.difference(mySetB);
// testSet3.showCollection();
// let testSet4 = mySetB.difference(mySetA);
// testSet4.showCollection();

// mySetA.isSubSet(mySetB);
// mySetB.isSubSet(mySetA);
